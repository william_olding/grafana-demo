CREATE TABLE `temperature` (
    `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `room` VARCHAR(255),
    `temp` int(6),
    `date` DATE
);

INSERT INTO `temperature` (`id`,`room`,`temp`,`date`) VALUES (1,'PATCH 1',30,'2020-01-12'),(2,'PATCH 2',43,'2020-01-13'),(3,'PATCH 1',22,'2020-02-16'),(4,'PATCH 2',10,'2020-02-20');